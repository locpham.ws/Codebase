package com.whatsoft.codebase.ui.activity;

import com.whatsoft.codebase.base.BaseFragmentActivity;
import com.whatsoft.codebase.base.BaseMainFragment;
import com.whatsoft.codebase.ui.fragment.HomeFragment;

/**
 * Created by mb on 7/14/16.
 */

public class MainActivity extends BaseFragmentActivity {

    @Override
    public BaseMainFragment getHomeFragment() {
        return new HomeFragment();
    }

    @Override
    public boolean hasLeftDrawer() {
        return false;
    }
}
