package com.whatsoft.codebase.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.whatsoft.codebase.R;
import com.whatsoft.codebase.base.BaseMainFragment;
import com.whatsoft.codebase.listener.OnClickOptionMenu;
import com.whatsoft.codebase.utils.ToastUtil;

import java.util.Random;

/**
 * Created by mb on 7/14/16.
 */

public class ChildFragment extends BaseMainFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Child " + new Random().nextInt(100));
        setOptionMenu(R.mipmap.ic_more_white, new OnClickOptionMenu() {
            @Override
            public void onClick() {
                ToastUtil.showSort("More");
            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new ChildFragment(), true);
            }
        });
    }
}
