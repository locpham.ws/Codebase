package com.whatsoft.codebase.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.whatsoft.codebase.R;
import com.whatsoft.codebase.base.BaseMainFragment;
import com.whatsoft.codebase.listener.OnItemClickOptionMenu;
import com.whatsoft.codebase.utils.ToastUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mb on 7/14/16.
 */

public class HomeFragment extends BaseMainFragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Home");
        List<String> menu = new ArrayList<>();
        menu.add("Option 1");
        menu.add("Option 2");
        menu.add("Option 3");
        menu.add("Option 4");
        menu.add("Option 5");
        setOptionMenu(menu, new OnItemClickOptionMenu() {
            @Override
            public void onItemClicked(String name, int position) {
                ToastUtil.showSort(name);
            }
        }, 0);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeFragment(new ChildFragment(), true);
            }
        });
    }
}
