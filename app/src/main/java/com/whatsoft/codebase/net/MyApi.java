package com.whatsoft.codebase.net;

import com.whatsoft.codebase.base.MessageResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Vinova on 12/15/16. Hello Vietnam
 */

public interface MyApi {
    interface Version {
        String V1 = "v1";
    }

    String Prefix = "/api/";

    @FormUrlEncoded
    @POST("/register_gcm")
    Observable<MessageResponse> registerPushNotification(@Field("device_token") String token, @Field("device_id") String deviceId,
                                                         @Field("platform") String os);

    @FormUrlEncoded
    @POST(Prefix + Version.V1 + "/unregister_gcm")
    Observable<MessageResponse> unregisterPushNotification(@Field("device_id") String deviceId);
}
