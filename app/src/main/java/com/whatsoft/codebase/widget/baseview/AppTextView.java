package com.whatsoft.codebase.widget.baseview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by mb on 7/14/16.
 */

public class AppTextView extends TextView{
    public AppTextView(Context context) {
        super(context);
    }

    public AppTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AppTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
