package com.whatsoft.codebase.widget.baseview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by mb on 7/14/16.
 */

public class AppEditText extends EditText {
    public AppEditText(Context context) {
        super(context);
    }

    public AppEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AppEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
