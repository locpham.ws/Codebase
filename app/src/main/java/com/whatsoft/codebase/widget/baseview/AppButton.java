package com.whatsoft.codebase.widget.baseview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by mb on 7/14/16.
 */

public class AppButton extends Button {
    public AppButton(Context context) {
        super(context);
    }

    public AppButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AppButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
