package com.whatsoft.codebase;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;
import com.whatsoft.codebase.di.component.AppComponent;
import com.whatsoft.codebase.di.component.DaggerAppComponent;
import com.whatsoft.codebase.di.module.AppModule;
import com.whatsoft.codebase.di.module.CommonModule;

/**
 * Created by mb on 7/14/16.
 */

public class MyApplication extends Application {
    private static MyApplication instance = null;

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule())
                .commonModule(new CommonModule())
                .build();
        configDBBrowser();
    }

    public static MyApplication getInstance() {
        return instance;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    private void configDBBrowser() {
        if (!BuildConfig.isDebug) {
            return;
        }

        RealmInspectorModulesProvider provider = RealmInspectorModulesProvider.builder(this)
                .withMetaTables()
                .withDescendingOrder()
                .build();

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(provider)
                        .build());
    }
}
