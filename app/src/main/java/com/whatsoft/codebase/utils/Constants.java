package com.whatsoft.codebase.utils;

/**
 * Created by Jerry  on 03/08/2016.
 */

public interface Constants {

    float IMAGE_RATE = 16f / 9f;

    String HTTP_AUTH_TOKEN = "Http-Auth-Token";
    String SHOW_DATE_FORMAT = "dd-MMMM-yyyy";
    String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'.000Z'"; //"2016-08-09T11:43:16.000Z"
    String SERVER_ONLY_DATE_FORMAT = "yyyy-MM-dd";
    String SERVER_MONTH_FORMAT = "yyyy-MM";

    int PER_PAGE = 20;
    int MAX_NUM_PHOTO_ISSUE = 10;
}
