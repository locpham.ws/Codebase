package com.whatsoft.codebase.utils;

import android.os.Build;
import android.text.Html;
import android.text.TextUtils;
import android.widget.TextView;

/**
 * Created by mb on 10/18/16.
 */

public class HtmlUtils {
    public static void fromHtml(TextView textView, String text) {
        if (textView == null) {
            return;
        }

        if (TextUtils.isEmpty(text)) {
            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY));
        } else {
            textView.setText(Html.fromHtml(text));
        }
    }
}
