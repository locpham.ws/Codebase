package com.whatsoft.codebase.utils;

import android.widget.Toast;

import com.whatsoft.codebase.MyApplication;

/**
 * Created by mb on 3/31/16.
 */
public class ToastUtil {
    public static void showSort(String message) {
        if (MyApplication.getInstance() == null) {
            return;
        }
        Toast.makeText(MyApplication.getInstance().getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}
