package com.whatsoft.codebase.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;

import com.whatsoft.codebase.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by mb on 3/29/16.
 */
public class Utils {
    public static int getScreenWidth(Activity context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int getScreenHeight(Activity context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    private static float scale;

    public static int dpToPixel(float dp, Context context) {
        if (scale == 0) {
            scale = context.getResources().getDisplayMetrics().density;
        }
        return (int) (dp * scale);
    }

    public static String formatDate(Date date, String format) {
        if (date == null) {
            return "";
        }
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
            return simpleDateFormat.format(date);
        } catch (Exception e) {
            Log.e(e);
        }
        return date.toString();
    }

    public static String formatDate(String strDate, String inputFormat, String outputFormat) {
        if (strDate == null) {
            return "";
        }
        try {
            Date date = parseDate(strDate, inputFormat);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(outputFormat, Locale.US);
            return simpleDateFormat.format(date);
        } catch (Exception e) {
            Log.e(e);
        }
        return strDate;
    }

    public static Date parseDate(String date, String format) {
        if (TextUtils.isEmpty(date)) {
            return new Date();
        }
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone(Locale.getDefault().getCountry()));
            return simpleDateFormat.parse(date);
        } catch (Exception e) {
            Log.e(e);
        }
        return new Date();
    }

    public static Date parseDateServer(String date, String format) {
        if (TextUtils.isEmpty(date)) {
            return new Date();
        }
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
            return simpleDateFormat.parse(date);
        } catch (Exception e) {
            Log.e(e);
        }
        return new Date();
    }

    public static Date getToday() {
        Calendar c = Calendar.getInstance();
        return c.getTime();
    }

    public static Date getServerToday() {
        TimeZone tz = TimeZone.getTimeZone(Locale.ROOT.getCountry());
        Calendar c = Calendar.getInstance(tz);
        return c.getTime();
    }

    public static String formatLeftTime(long milisecond) {
        long second = milisecond / 1000;
        long minute = second / 60;
        long hours = minute / 60;
        long leftMinute = minute % 60;
        if (hours == 0) {
            return leftMinute + " minutes";
        } else if (leftMinute == 0) {
            return hours + " hours";
        } else {
            return hours + " hours " + leftMinute + " minutes";
        }
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static String getAuthenString() {
        return SharedPreferenceHelper.getInstance().getString(Constants.HTTP_AUTH_TOKEN, "");
    }

    public static File copyImageUri2File(Context context, Uri uri) {
        try {
            File file = File.createTempFile("image", ".jpg", context.getCacheDir());
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            copyInputStreamToFile(inputStream, file);
            return file;
        } catch (Exception e) {
            Log.e(e);
        }
        return null;
    }

    private static void copyInputStreamToFile(InputStream in, File file) throws IOException {
        OutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[10 * 1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        out.close();
        in.close();
    }

    public static String getImagePath(Context context, Uri uri) {
        String result = null;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
            cursor.close();
        }
        return result;
    }

    public static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        String mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        Log.d(mCurrentPhotoPath);
        return image;
    }

    public static String dispatchTakePictureIntent(Activity activity, int requestCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = Utils.createImageFile();
            } catch (IOException ex) {
                Log.e(ex);
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                activity.startActivityForResult(takePictureIntent, requestCode);
                return photoFile.getAbsolutePath();
            }
        }
        return "";
    }

    public static long getAvailableMemory(Context context) {
//        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
//        ActivityManager activityManager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
//        activityManager.getMemoryInfo(mi);
//        long m = mi.availMem / 1024;// to kilobyte
        long m = Runtime.getRuntime().freeMemory() / 1024;
        Log.d("Available Memory: " + m + " KB");
        return m;
    }

    public static String getDeviceId(Context context) {
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("Android device id: " + android_id);
        return android_id;
    }

    public static int getNotificationId() {
        long time = new Date().getTime();
        String tmpStr = String.valueOf(time);
        if (tmpStr.length() <= 8) {
            return Integer.valueOf(tmpStr);
        }
        String last4Str = tmpStr.substring(tmpStr.length() - 8);
        return Integer.valueOf(last4Str);
    }

    public static String upperCaseFirstChar(String s) {
        if (TextUtils.isEmpty(s)) {
            return s;
        }

        String string = s.trim();
        if (string.length() == 0) {
            return string;
        }

        if (string.length() == 1) {
            return string.toUpperCase();
        }

        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }

    public static String upperCaseAllFirstChar(String s) {
        if (TextUtils.isEmpty(s)) {
            return s;
        }

        String[] strings = s.split(" ");
        String res = "";
        for (String string : strings) {
            if (!TextUtils.isEmpty(res)) {
                res += " ";
            }
            res += upperCaseFirstChar(string.trim());
        }
        return res;
    }

    public static String convert2String(double values) {
        DecimalFormat format = new DecimalFormat("0.###");
        return format.format(values);
    }

    public static int between2Date(Date date1, Date date2) {
        long time = date2.getTime() - date1.getTime();
        return (int) (time / 1000 / 60 / 60 / 24);
    }

    /**
     * compare two date
     *
     * @param date1 date 1
     * @param date2 date 2
     * @return 0 date1 = dat2 1 dat1 < date2 -1 date 1 > date2
     */
    public static int compare2Date(Calendar date1, Calendar date2) {
        if (date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR)) {
            if (date1.get(Calendar.DAY_OF_YEAR) == date2.get(Calendar.DAY_OF_YEAR)) {
                return 0;
            } else if (date1.get(Calendar.DAY_OF_YEAR) > date2.get(Calendar.DAY_OF_YEAR)) {
                return 1;
            } else {
                return -1;
            }
        } else if (date1.get(Calendar.YEAR) < date2.get(Calendar.YEAR)) {
            return -1;
        } else {
            return 1;
        }
    }

    public static String groupString(String str, int numberChar, String separate) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        String result = "";
        String sub = str.substring(0, numberChar);
        int start = numberChar;
        while (!TextUtils.isEmpty(sub)) {
            if (start != numberChar) {
                result += separate;
            }
            result += sub;
            if (start >= str.length()) {
                break;
            }

            if (start + numberChar >= str.length()) {
                sub = str.substring(start);
            } else {
                sub = str.substring(start, start + numberChar);
            }
            start += numberChar;
        }
        return result;
    }

    public static String getTimeAgo(String time) {
        String timeAgo = "";
        try {

            Date now = Calendar.getInstance().getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);

            SimpleDateFormat sdfDue = new SimpleDateFormat(Constants.SERVER_DATE_FORMAT, Locale.US);
            sdfDue.setTimeZone(TimeZone.getTimeZone("Singapore"));
            Date due = sdfDue.parse(time);
            if (due.getTime() > now.getTime()) {
                timeAgo = "a few seconds ago";
            } else {
                timeAgo = DateUtils.getRelativeTimeSpanString(due.getTime(), now.getTime(), DateUtils.SECOND_IN_MILLIS).toString();
            }

        } catch (Exception exp) {
            timeAgo = time;
            android.util.Log.d("Utils", "getTimeAgo: " + exp.getMessage());
        }
        return timeAgo;
    }

    public static String ellapseTime(Context context, Date startDate, Date endDate) {

        long ellapse = Math.abs(endDate.getTime() - startDate.getTime());

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long ellapseDays = ellapse / daysInMilli;
        ellapse = ellapse % daysInMilli;

        long ellapseHours = ellapse / hoursInMilli;
        ellapse = ellapse % hoursInMilli;

        long ellapseMinutes = ellapse / minutesInMilli;

        String s = "";

        String now = context.getString(R.string.now);
        String mago = context.getString(R.string.m_ago);
        String hmago = context.getString(R.string.hm_ago);
        String yesterday = context.getString(R.string.yesterday);

        if (ellapseDays < 1) {
            if (ellapseHours == 0 && ellapseMinutes <= 1) {
                s = now;
            } else if (ellapseHours == 0 && ellapseMinutes > 1) {
                s = String.format(mago, String.valueOf(ellapseMinutes));
            } else {
                s = String.format(hmago, String.valueOf(ellapseHours), String.valueOf(ellapseMinutes));
            }
        }
        if (ellapseDays == 1) {
            s = yesterday;
        }
        if (ellapseDays > 1) {
            s = Utils.formatDate(startDate, "dd MMM");
        }
        return s;

    }

    public static String ellapseTimeIssue(Context context, Date startDate, Date endDate) {

        long ellapse = Math.abs(endDate.getTime() - startDate.getTime());

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long ellapseDays = ellapse / daysInMilli;
        ellapse = ellapse % daysInMilli;

        long ellapseHours = ellapse / hoursInMilli;
        ellapse = ellapse % hoursInMilli;

        long ellapseMinutes = ellapse / minutesInMilli;


        if (ellapseDays < 1) {
            if (ellapseHours == 0 && ellapseMinutes <= 1) {
                return String.format(context.getString(R.string.minute_ago), 1);
            } else if (ellapseHours == 0 && ellapseMinutes > 1) {
                return String.format(context.getString(R.string.minutes_ago), ellapseMinutes);
            } else {
                return String.format(context.getString(R.string.hour_min_ago), ellapseHours, ellapseHours > 1 ? "s" : "", ellapseMinutes, ellapse > 1 ? "s" : "");
            }
        }
        if (ellapseDays == 1) {
            return context.getString(R.string.yesterday);
        }
        if (ellapseDays <= 7) {
            return String.format(context.getString(R.string.days_ago), ellapseDays);
        }
        if (ellapseDays > 7) {
            return Utils.formatDate(startDate, "dd MMM yyyy");
        }
        return "";

    }


    public static String formatNumber(long number, String format) {
        DecimalFormat decimalFormat = new DecimalFormat(format);
        return decimalFormat.format(number);
    }

    public static String formatNumber(double number, String format) {
        DecimalFormat decimalFormat = new DecimalFormat(format);
        return decimalFormat.format(number);
    }

    public static String formatBigCurencyNumber(Context context, double number) {
        double bi = 1000000000f, mi = 1000000f, th = 1000f;
        if (number / bi > 1) {
            return formatNumber(number / bi, "#,###.#") + context.getString(R.string.billion);
        }

        if (number / mi > 1) {
            return formatNumber(number / mi, "#,###.#") + context.getString(R.string.million);
        }

        if (number / th > 1) {
            return formatNumber(number / th, "#,###.#") + context.getString(R.string.thousand);
        }

        return formatNumber(number, "#,###.#");
    }

    public static float convertDpToPixel(float dp, Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }

    public static String getStringServerCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.SERVER_DATE_FORMAT, Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(Locale.getDefault().getCountry()));
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String getStringServerCurrentTime(String format) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(Locale.getDefault().getCountry()));
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String getStringServerTime(Calendar calendar) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.SERVER_DATE_FORMAT, Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(Locale.getDefault().getCountry()));
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String getStringCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.SERVER_DATE_FORMAT, Locale.US);
        String time = simpleDateFormat.format(calendar.getTime());
        return time;
    }

    public static String getStringCurrentTime(String format) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.US);
        String time = simpleDateFormat.format(calendar.getTime());
        return time;
    }


    public static float getDisplayDensity(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.density;
    }


    public static int saturdaySundayCount(Date d1, Date d2) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(d2);

        int sundays = 0;
        int saturday = 0;

        while (!c1.after(c2)) {
            if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                saturday++;
            }
            if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                sundays++;
            }

            c1.add(Calendar.DATE, 1);
        }

        return saturday + sundays;
    }

    public static boolean checkInternetConnection(Context context) {

        ConnectivityManager con_manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        if (con_manager.getActiveNetworkInfo() != null
                && con_manager.getActiveNetworkInfo().isAvailable()
                && con_manager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isSameMonth(Calendar c1, Calendar c2) {
        if (c1 == null || c2 == null)
            return false;
        return (c1.get(Calendar.ERA) == c2.get(Calendar.ERA)
                && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
                && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH));
    }

    public static boolean isSameDate(Calendar c1, Calendar c2) {
        if (c1 == null || c2 == null)
            return false;
        return (c1.get(Calendar.ERA) == c2.get(Calendar.ERA)
                && c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
                && c1.get(Calendar.MONTH) == c2.get(Calendar.MONTH)
                && c1.get(Calendar.DAY_OF_MONTH) == c2.get(Calendar.DAY_OF_MONTH));
    }

    public static Address getCompleteAddress(Context context, String addressName) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocationName(addressName, 1);
            if (addresses != null && addresses.size() > 0) {
                Log.d("Address: " + addresses.toString());
                return addresses.get(0);
            } else {
                Log.w("My Current location address : No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current location address: Cannot get Address!");
        }
        return null;
    }

    public static long timeStringToStamp(String time, String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parseDate(time, format));
        return calendar.getTimeInMillis();
    }
}
