package com.whatsoft.codebase.listener;

import android.text.Editable;

/**
 * Created by mb on 8/12/16.
 */

public abstract class TextWatcher implements android.text.TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }
}
