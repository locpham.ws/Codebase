package com.whatsoft.codebase.listener;

import android.support.v4.view.ViewPager;

/**
 * Created by mb on 8/15/16.
 */

public abstract class OnPageChangeListener implements ViewPager.OnPageChangeListener {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
