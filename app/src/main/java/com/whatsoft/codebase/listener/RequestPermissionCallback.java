package com.whatsoft.codebase.listener;

/**
 * Created by mb on 7/29/16.
 */

public interface RequestPermissionCallback {
    void onAccepted();

    void onDenied();
}
