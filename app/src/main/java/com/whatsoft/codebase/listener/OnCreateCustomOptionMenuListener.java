package com.whatsoft.codebase.listener;

import android.view.View;

/**
 * Created by mb on 8/29/16.
 */

public interface OnCreateCustomOptionMenuListener {
    void onViewCreated(View view);
}
