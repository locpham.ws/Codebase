package com.whatsoft.codebase.gcm;

/**
 * Created by mb on 9/27/16.
 */

public interface NotificationType {
    String NORMAL = "normal";
    String NEWS = "news";
    String ISSUE = "issues";
    String APPOINTMENT = "appointments";
}
