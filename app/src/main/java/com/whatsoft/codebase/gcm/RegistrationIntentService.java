package com.whatsoft.codebase.gcm;

/**
 * Created by mb on 9/27/16.
 */

//public class RegistrationIntentService extends IntentService {
//    public static final String EXTRA_TYPE = "type";
//
//    public RegistrationIntentService() {
//        super("RegistrationIntentService");
//    }
//
//    @Override
//    protected void onHandleIntent(Intent intent) {
//        int type = intent.getIntExtra(EXTRA_TYPE, 0);
//        InstanceID instanceID = InstanceID.getInstance(this);
//        try {
//            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
//                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
//            switch (type) {
//                case 0:
//                    //register
//                    CacheUtils.setGCMToken(token);
//                    GcmManager.getInstance(this).registerGcmToServer(token, Utils.getDeviceId(getApplicationContext()));
//                    break;
//                case 1:
//                    //unregister
//                    try {
//                        instanceID.deleteToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE);
//                        GcmManager.getInstance(this).unregisterGcmToServer(CacheUtils.getGCMToken(), Utils.getDeviceId(getApplicationContext()));
//                        CacheUtils.setGCMToken("");
//                        Log.d("unregister true");
//                    } catch (IOException e) {
//                        Log.d("unregister false");
//                    }
//                    break;
//            }
//        } catch (IOException e) {
//            Log.e(e);
//        }
//    }
//}
