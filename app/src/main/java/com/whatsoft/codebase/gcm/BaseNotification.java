package com.whatsoft.codebase.gcm;

/**
 * Created by Sandy on 7/16/15.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

import com.whatsoft.codebase.R;
import com.whatsoft.codebase.ui.activity.MainActivity;
import com.whatsoft.codebase.utils.CacheUtils;
import com.whatsoft.codebase.utils.Log;
import com.whatsoft.codebase.utils.Utils;


public abstract class BaseNotification {
    public static final String TYPE = "type";
    public static final String DATA = "data";
    public static final String MESSAGE = "message";

    public void show(Context context, String data, String type, String message) {
        if (!CacheUtils.isConfigPushEnable()) {
            return;
        }
        try {
            Intent intent;
            intent = new Intent(context, MainActivity.class);

            intent.putExtra(TYPE, type);
            intent.putExtra(DATA, data);
            intent.putExtra(MESSAGE, message);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(intent);
            PendingIntent pendingIntent = stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setStyle(new NotificationCompat.BigTextStyle())
                    .setContentText(message);

            if (CacheUtils.isConfigPushSound()) {
                builder.setSound(uri);
            } else {
                builder.setDefaults(0);
            }
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(Utils.getNotificationId(), builder.build());
        } catch (Exception e) {
            Log.e(e);
        }
    }
}