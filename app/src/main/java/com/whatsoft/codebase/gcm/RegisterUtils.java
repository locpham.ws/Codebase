package com.whatsoft.codebase.gcm;

import android.support.annotation.NonNull;

import com.whatsoft.codebase.base.MessageResponse;
import com.whatsoft.codebase.base.ResponseListener;
import com.whatsoft.codebase.net.MyApi;
import com.whatsoft.codebase.utils.CacheUtils;
import com.whatsoft.codebase.utils.Log;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RegisterUtils {
    public static void postDevice(MyApi myApi, String gcmId, String device_id, boolean isDelete) {
        if (!isDelete) {
            CacheUtils.setConfigPushEnable(true);
            Observable<MessageResponse> observable = myApi.registerPushNotification(
                    gcmId, device_id, "android");
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ResponseListener<MessageResponse>() {
                        @Override
                        public void onSuccess(MessageResponse result) {
                            Log.d("Register server ok");
                            CacheUtils.setRegisterPushNot2Server(true);
                        }

                        @Override
                        public void onError(@NonNull MessageResponse messageResponse) {
                            Log.d("Register server error");
                            CacheUtils.setRegisterPushNot2Server(false);
                        }
                    });
        } else {
            CacheUtils.setConfigPushEnable(false);
            Observable<MessageResponse> observable = myApi.unregisterPushNotification(device_id);
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new ResponseListener<MessageResponse>() {
                        @Override
                        public void onSuccess(MessageResponse result) {
                            Log.d("UnRegister server ok");
                            CacheUtils.setUnregisterPushNot2Server(true);
                        }

                        @Override
                        public void onError(@NonNull MessageResponse messageResponse) {
                            Log.d("UnRegister server error");
                            CacheUtils.setUnregisterPushNot2Server(false);
                        }
                    });
        }
    }
}
