package com.whatsoft.codebase.gcm;

import android.content.Context;

import com.whatsoft.codebase.MyApplication;
import com.whatsoft.codebase.net.MyApi;
import com.whatsoft.codebase.utils.Log;

import javax.inject.Inject;

public class GcmManager {
    private Context context;

    @Inject
    MyApi myApi;

    private static GcmManager gcmManager = null;

    public static GcmManager getInstance(Context context) {
        if (gcmManager == null) {
            gcmManager = new GcmManager(context);
        }
        return gcmManager;
    }

    public GcmManager(Context context) {
        this.context = context;
        MyApplication.getInstance().getAppComponent().inject(this);
    }

    public void registerGCM() {
//        Intent intent = new Intent(context, RegistrationIntentService.class);
//        intent.putExtra(RegistrationIntentService.EXTRA_TYPE, 0);
//        context.startService(intent);
    }

    public void unRegisterGCM() {
//        Intent intent = new Intent(context, RegistrationIntentService.class);
//        intent.putExtra(RegistrationIntentService.EXTRA_TYPE, 1);
//        context.startService(intent);
    }

    public void registerGcmToServer(String regId, String device_id) {
        Log.d("Register GCM");
        RegisterUtils.postDevice(myApi, regId, device_id, false);
    }

    public void unregisterGcmToServer(String regId, String device_id) {
        Log.d("Unregister GCM");
        RegisterUtils.postDevice(myApi, regId, device_id, true);
    }
}
