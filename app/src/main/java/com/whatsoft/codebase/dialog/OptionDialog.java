package com.whatsoft.codebase.dialog;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.whatsoft.codebase.R;
import com.whatsoft.codebase.base.BaseDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by mb on 8/4/16.
 */

public class OptionDialog extends BaseDialog {

    @Bind(R.id.message_text)
    TextView tvMessage;

    @Bind(R.id.btn_negative)
    Button btnClose;

    @Bind(R.id.btn_option_1)
    Button btnOption1;

    @Bind(R.id.btn_option_2)
    Button btnOption2;

    @Bind(R.id.imv_icon)
    ImageView imvIcon;

    private String message = "";
    private String negativeButton, optionOne, optionTwo;
    private int icon;
    private OnCloseDialog onCloseDialog;

    public OptionDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_option);
        ButterKnife.bind(this);
    }

    public OptionDialog setMessage(String message) {
        if (!TextUtils.isEmpty(message)) {
            this.message = message;
        }
        return this;
    }

    public OptionDialog setNegativeButton(String negativeButton) {
        this.negativeButton = negativeButton;
        return this;
    }

    public OptionDialog setOption1(String positiveButton) {
        this.optionOne = positiveButton;
        return this;
    }

    public OptionDialog setOption2(String positiveButton) {
        this.optionTwo = positiveButton;
        return this;
    }

    public OptionDialog setIcon(int icon) {
        this.icon = icon;
        return this;
    }

    @Override
    public void show() {
        super.show();
        if (icon > 0) {
            imvIcon.setImageResource(icon);
        }
        if (!TextUtils.isEmpty(message)) {
            tvMessage.setText(Html.fromHtml(message));
        }
        if (!TextUtils.isEmpty(negativeButton)) {
            btnClose.setText(Html.fromHtml(negativeButton));
            btnClose.setVisibility(View.VISIBLE);
        } else {
            btnClose.setText(Html.fromHtml(""));
            btnClose.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(optionOne)) {
            btnOption1.setVisibility(View.VISIBLE);
            btnOption1.setText(Html.fromHtml(optionOne));
        } else {
            btnOption1.setText(Html.fromHtml(""));
            btnOption1.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(optionTwo)) {
            btnOption2.setVisibility(View.VISIBLE);
            btnOption2.setText(Html.fromHtml(optionTwo));
        } else {
            btnOption2.setText(Html.fromHtml(""));
            btnOption2.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_negative)
    void clickCancel() {
        dismiss();
        if (onCloseDialog != null) {
            onCloseDialog.onClose();
        }
    }

    @OnClick(R.id.btn_option_1)
    void onClickOption1() {
        dismiss();
        if (onCloseDialog != null) {
            onCloseDialog.onOption1();
        }
    }

    @OnClick(R.id.btn_option_2)
    void onClickOption2() {
        dismiss();
        if (onCloseDialog != null) {
            onCloseDialog.onOption2();
        }
    }

    public interface OnCloseDialog {
        void onOption1();

        void onOption2();

        void onClose();
    }

    public OptionDialog setOnCloseDialog(OnCloseDialog onCloseDialog) {
        this.onCloseDialog = onCloseDialog;
        return this;
    }
}