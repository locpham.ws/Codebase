package com.whatsoft.codebase.dialog;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.whatsoft.codebase.R;
import com.whatsoft.codebase.base.BaseDialog;
import com.whatsoft.codebase.utils.HtmlUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jerry  on 30/03/2016.
 */
public class MessageDialog extends BaseDialog {

    @Bind(R.id.message_text)
    TextView tvMessage;

    @Bind(R.id.btn_negative)
    Button btnClose;

    @Bind(R.id.btn_positive)
    Button btnOk;

    @Bind(R.id.imv_icon)
    ImageView imvIcon;

    private String message = "";
    private String negativeButton, positiveButton;
    private int icon;
    private boolean isHtml = true;

    public MessageDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_message);
        ButterKnife.bind(this);
    }

    public MessageDialog setMessage(String message) {
        if (!TextUtils.isEmpty(message)) {
            this.message = message;
        }
        return this;
    }

    public MessageDialog setNegativeButton(String negativeButton) {
        this.negativeButton = negativeButton;
        return this;
    }

    public MessageDialog setPositiveButton(String positiveButton) {
        this.positiveButton = positiveButton;
        return this;
    }

    public MessageDialog setIcon(int icon) {
        this.icon = icon;
        return this;
    }

    public MessageDialog setHtml(boolean html) {
        isHtml = html;
        return this;
    }

    @Override
    public void show() {
        super.show();
        if (icon > 0) {
            imvIcon.setImageResource(icon);
        }
        if (!TextUtils.isEmpty(message)) {
            if (isHtml) {
                HtmlUtils.fromHtml(tvMessage, message);
            } else {
                tvMessage.setText(message);
            }
        }
        if (!TextUtils.isEmpty(negativeButton)) {
            HtmlUtils.fromHtml(btnClose, negativeButton);
            btnClose.setVisibility(View.VISIBLE);
        } else {
            btnClose.setText("");
            btnClose.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(positiveButton)) {
            btnOk.setVisibility(View.VISIBLE);
            HtmlUtils.fromHtml(btnOk, positiveButton);
        } else {
            btnOk.setText("");
            btnOk.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_negative)
    void clickCancel() {
        dismiss();
        if (getListener() != null) {
            getListener().onNegative();
        }
    }

    @OnClick(R.id.btn_positive)
    void onClickOk() {
        dismiss();
        if (getListener() != null) {
            getListener().onPositive();
        }
    }
}
