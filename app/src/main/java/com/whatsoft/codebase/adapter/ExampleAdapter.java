package com.whatsoft.codebase.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.whatsoft.codebase.base.BaseActivity;
import com.whatsoft.codebase.base.BaseAdapter;
import com.whatsoft.codebase.base.BaseViewHolder;
import com.whatsoft.codebase.models.ExampleObject;

/**
 * Created by Vinova on 12/16/16. Hello Vietnam
 */

public class ExampleAdapter extends BaseAdapter<ExampleObject, ExampleAdapter.ViewHolder> {
    public ExampleAdapter(BaseActivity mActivity) {
        super(mActivity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    class ViewHolder extends BaseViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bind(int position) {

        }
    }
}
