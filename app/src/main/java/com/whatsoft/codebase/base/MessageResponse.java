package com.whatsoft.codebase.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.whatsoft.codebase.utils.Utils;

import java.util.List;

/**
 * Created by mb on 3/30/16.
 */
public class MessageResponse {

    @Expose
    @SerializedName("code")
    private String code;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("messages")
    private List<String> messages;

    private int statusCode;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return Utils.upperCaseFirstChar(message);
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
