package com.whatsoft.codebase.base;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<D> {

    @SerializedName("data")
    private D data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }
}