package com.whatsoft.codebase.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Vinova on 12/16/16. Hello Vietnam
 */

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bind(int position);
}
