package com.whatsoft.codebase.base;

import com.whatsoft.codebase.listener.OnClickOptionMenu;
import com.whatsoft.codebase.listener.OnCreateCustomOptionMenuListener;
import com.whatsoft.codebase.listener.OnItemClickOptionMenu;

import java.util.List;

/**
 * Created by Vinova on 12/15/16. Hello Vietnam
 */

public interface IBaseActivity {
    void setEnableOptionMenu(boolean enableOptionMenu);

    void setTitle(String title);

    void changeFragment(BaseMainFragment fragment, boolean addBackStack);

    void setOptionMenu(String title, OnClickOptionMenu listener);

    void setOptionMenu(int icon, OnClickOptionMenu listener);

    void setShowHomeMenu(boolean isShow);

    void hideLeftButton();

    void showLeftButton();

    void hideToolBar(boolean isShow);

    void setOptionMenu(List<String> menus, OnItemClickOptionMenu listener, int selectedPosition);

    void setCustomOptionMenu(int resId, OnCreateCustomOptionMenuListener onCreateCustomOptionMenuListener);

    void setLeftText(String text, OnClickOptionMenu onClickOptionMenu);

    void setOptionMenuColor(int color);
}
