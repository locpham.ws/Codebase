package com.whatsoft.codebase.di.component;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.whatsoft.codebase.base.BaseActivity;
import com.whatsoft.codebase.base.BaseFragment;
import com.whatsoft.codebase.di.module.AppModule;
import com.whatsoft.codebase.di.module.CommonModule;
import com.whatsoft.codebase.di.scope.AppScope;
import com.whatsoft.codebase.gcm.GcmManager;

import javax.inject.Named;

import dagger.Component;

/**
 * Created by mb on 7/27/16.
 */
@AppScope
@Component(modules = {AppModule.class, CommonModule.class})
public interface AppComponent {

    void inject(BaseActivity baseActivity);

    void inject(BaseFragment baseFragment);

    void inject(GcmManager baseFragment);

    Context context();

    @Named("vertical_linear")
    LinearLayoutManager vertical_linear();

    @Named("horizontal_linear")
    LinearLayoutManager horizontal_linear();
}
