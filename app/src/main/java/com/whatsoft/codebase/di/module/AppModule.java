package com.whatsoft.codebase.di.module;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.whatsoft.codebase.BuildConfig;
import com.whatsoft.codebase.MyApplication;
import com.whatsoft.codebase.di.scope.AppScope;
import com.whatsoft.codebase.net.HttpsTrustManager;
import com.whatsoft.codebase.net.MyApi;
import com.whatsoft.codebase.net.ToStringConverterFactory;
import com.whatsoft.codebase.utils.CacheUtils;
import com.whatsoft.codebase.utils.Log;

import org.greenrobot.eventbus.EventBus;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mb on 7/27/16.
 */
@Module
@AppScope
public class AppModule {

    public AppModule() {
    }

    @Provides
    @AppScope
    public MyApi provideApi(OkHttpClient okHttpClient) {
        synchronized (MyApi.class) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(new ToStringConverterFactory())
                    .client(okHttpClient)
                    .build();
            return retrofit.create(MyApi.class);
        }
    }

    @Provides
    @AppScope
    protected OkHttpClient provideOkHttp(TrustManager[] trustManager, SSLSocketFactory sslSocketFactory) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Response response;
                if (!TextUtils.isEmpty(CacheUtils.getAuthToken())) {
                    Request newRequest = request.newBuilder()
                            .addHeader("Http-Auth-Token", CacheUtils.getAuthToken())
                            .build();
                    response = chain.proceed(newRequest);
                } else {
                    response = chain.proceed(request);
                }
                return response;
            }
        });
        if (trustManager != null && trustManager.length > 0) {
            httpClient.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustManager[0]);
        } else {
            httpClient.sslSocketFactory(sslSocketFactory, new HttpsTrustManager());
        }
        httpClient.writeTimeout(15 * 60 * 1000, TimeUnit.MILLISECONDS);
        httpClient.readTimeout(60 * 1000, TimeUnit.MILLISECONDS);
        httpClient.connectTimeout(20 * 1000, TimeUnit.MILLISECONDS);
        return httpClient.build();
    }

    @Provides
    @AppScope
    protected Application provideApplication() {
        return MyApplication.getInstance();
    }

    @Provides
    public Realm provideRealm(RealmConfiguration realmConfiguration) {
        Realm.setDefaultConfiguration(realmConfiguration);
        return Realm.getDefaultInstance();
    }

    @Provides
    @AppScope
    protected RealmConfiguration provideRealmConfig(Application application, RealmMigration realmMigration) {
        return new RealmConfiguration
                .Builder(application)
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .migration(realmMigration)
                .build();
    }

    @Provides
    @AppScope
    protected RealmMigration provideRealmMigration() {
        return new com.whatsoft.codebase.base.RealmMigration();
    }

    @Provides
    @AppScope
    public EventBus provideEventBus() {
        return EventBus.getDefault();
    }

    @Provides
    @AppScope
    public Context provideContext() {
        return MyApplication.getInstance();
    }

    @Provides
    @AppScope
    protected TrustManager[] provideTrustManager(Context context) {
        try {
            // Load CAs from an InputStream
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = new BufferedInputStream(context.getAssets().open("ssl.crt"));
            Certificate ca = null;
            try {
                ca = cf.generateCertificate(caInput);
            } catch (Exception e) {
                Log.e(e);
            } finally {
                caInput.close();
            }

            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            return tmf.getTrustManagers();
        } catch (Exception e) {
            Log.e(e);
            return null;
        }
    }

    @Provides
    @AppScope
    protected SSLSocketFactory provideSslSocketFactory(TrustManager[] trustManagers) {
        // Create an SSLContext that uses our TrustManager
        SSLContext sslContext;
        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustManagers, null);
            return sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException e) {
            Log.e(e);
        } catch (KeyManagementException e) {
            Log.e(e);
        }
        return null;
    }
}
